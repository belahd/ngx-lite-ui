import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AvatarModule } from './avatar/avatar.module';
import { BubbleModule } from './bubble/bubble.module';
import { ButtonModule } from './button/button.module';
import { CardModule } from './card/card.module';
import { CheckboxModule } from './checkbox/checkbox.module';
import { DatePickerModule } from './date-picker/date-picker.module';
import { GridModule } from './grid/grid.module';
import { IconModule } from './icon/icon.module';
import { LabelModule } from './label/label.module';
import { PagerModule } from './pager/pager.module';
import { PopupModule } from './popup/popup.module';
import { ProgressbarModule } from './progressbar/progressbar.module';
import { RadioModule } from './radio/radio.module';
import { SchedulerModule } from './scheduler/scheduler.module';
import { SwitchModule } from './switch/switch.module';
import { TabModule } from './tab/tab.module';
import { TextFieldModule } from './text-field/text-field.module';
import { SpreadsheetModule } from './spreadsheet/spreadsheet.module';
import { ToastModule } from './toast/toast.module';
import { SkeletonModule } from './loading/skeleton.module';
import { PopperModule } from './popper/popper.module';
import { DropdownMenuModule } from './dropdown/dropdown-menu.module';
import { DropdownModule } from './dropdown/dropdown.module';

const IMPORTED_MODULES = [
  LabelModule,
  TextFieldModule,
  ButtonModule,
  GridModule,
  CardModule,
  SchedulerModule,
  PopupModule,
  SwitchModule,
  ProgressbarModule,
  TabModule,
  IconModule,
  PagerModule,
  CheckboxModule,
  RadioModule,
  BubbleModule,
  AvatarModule,
  DatePickerModule,
  SpreadsheetModule,
  ToastModule,
  SkeletonModule,
  PopperModule,
  DropdownMenuModule,
  DropdownModule,
];

@NgModule({
  imports: [CommonModule, IMPORTED_MODULES],
  exports: IMPORTED_MODULES,
})
export class NgxLiteUiModule {}
