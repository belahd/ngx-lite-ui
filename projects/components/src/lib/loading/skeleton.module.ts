import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Skeleton } from './skeleton';
import { SkeletonDirective } from './skeleton.directive';

const EXPORTED_DECLARATIONS = [Skeleton, SkeletonDirective];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class SkeletonModule {}
