import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TextField } from './text-field';
import { LabelModule } from '../label/label.module';

const EXPORTED_DECLARATIONS = [TextField];

@NgModule({
  imports: [CommonModule, LabelModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class TextFieldModule {}
