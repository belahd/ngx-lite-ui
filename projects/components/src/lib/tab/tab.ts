import { Component, ContentChild, Input } from '@angular/core';
import { TabLabel } from "./tab-label";
import { TabContent } from "./tab-content";

@Component({
  selector: 'ngx-tab',
  template: '<div class="tab"><ng-content></ng-content></div>',
  styleUrls: ['tab.scss'],
})
export class Tab {
  @Input() title: string = '';
  @Input() isSelected: boolean = false;

  @ContentChild(TabLabel) label!: TabLabel;
  @ContentChild(TabContent) content!: TabContent;
}

