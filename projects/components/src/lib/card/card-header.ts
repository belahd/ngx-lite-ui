import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-card-header',
  template: `<div [ngClass]="css">
    <div class="card-header">
      <ng-content></ng-content>
    </div>
  </div>`,
  styleUrls: ['card.scss'],
})
export class CardHeader {
  @Input() css: string = '';
}
