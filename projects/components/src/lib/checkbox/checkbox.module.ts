import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Checkbox } from './checkbox';

const EXPORTED_DECLARATIONS = [Checkbox];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class CheckboxModule { }
