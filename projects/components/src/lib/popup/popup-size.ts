export enum PopupSize {
  ExtraLarge = 'xlarge',
  Large = 'large',
  Medium = 'medium',
  Small = 'small',
}
