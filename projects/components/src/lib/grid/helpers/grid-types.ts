import { TemplateRef } from '@angular/core';

export type ColumnType = {
  name: string;
  caption: string;
  visible: boolean;
  template?: TemplateRef<any>;
  sortable: boolean;
  direction: string;
  filterable: boolean;
};
