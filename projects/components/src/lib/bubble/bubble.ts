import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ngx-bubble',
  template: `<div 
                class="bubble background" 
                [ngClass]="color" 
                [class.hover]="isClickable" 
                (click)="onClick($event)">
                {{value}}
              </div>`,
  styleUrls: ['../core/styles.scss', 'bubble.scss'],
})

export class Bubble implements OnInit {
  @Input() color: string = '_default';
  @Input() value: number | null = null;

  isClickable: boolean = false;
  @Output() click: EventEmitter<any> = new EventEmitter<any>();

  onClick = (event: any): void => {
    event.stopPropagation();
    this.click.emit(event);
  }

  ngOnInit(): void {
    this.isClickable = this.click.observed;
  }
}
