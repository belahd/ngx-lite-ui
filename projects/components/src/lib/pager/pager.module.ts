import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from '../icon/icon.module';
import { DropdownModule } from '../dropdown/dropdown.module';
import { Pager } from './pager';

const EXPORTED_DECLARATIONS = [Pager];

@NgModule({
  imports: [CommonModule, IconModule, DropdownModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class PagerModule {}
