import {
  Component,
  EventEmitter,
  Injector,
  Input,
  Output,
} from '@angular/core';
import { PopupService } from './popup.service';
import { PopupSize } from './popup-size';

@Component({
  selector: 'ngx-popup',
  templateUrl: 'popup.html',
  styleUrls: ['popup.scss'],
})
export class Popup {
  params: any;

  @Input() header: string = 'Popup';
  @Input() size: PopupSize = PopupSize.Medium;
  @Input() doubleSided: boolean = false;
  @Input() leftWidth: string = '';
  @Input() save: string = 'Save';
  @Input() cancel: string = 'Cancel';
  @Input() saveType: string = 'primary';

  @Output() onSave: EventEmitter<{ close: () => void }> = new EventEmitter<{
    close: () => void;
  }>();
  @Output() onCancel: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private injector: Injector) {}

  handleSave() {
    this.onSave.emit({ close: () => this.injector.get(PopupService)?.close() });
  }

  handleClose() {
    this.onCancel.emit();
    this.injector.get(PopupService)?.close();
  }
}
