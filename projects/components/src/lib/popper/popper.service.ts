export class PopperService {
  private _id: string = '';

  get id(): string {
    return this._id;
  }

  set id(v: string) {
    this._id = v;
  }
}
