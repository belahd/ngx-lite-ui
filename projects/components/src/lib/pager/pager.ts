import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { PagerService } from './pager.service';



@Component({
  selector: 'ngx-pager',
  templateUrl: 'pager.html',
  styleUrls: ['pager.scss'],
})
export class Pager implements OnDestroy {

  @Input() limits: any = [10, 15, 25, 50];

  @Output() refresh = new EventEmitter();
  @Output() load = new EventEmitter();

  subscription: any;

  constructor(public pager: PagerService,) {
    this.subscription = pager.message.subscribe((page) => this.goto(page));
  }

  first = (): void => {
    if (this.pager.page != 0) {
      this.goto(0);
    }
  };

  previous = (): void => {
    const page = this.pager.page - 1;
    if (page >= 0) {
      this.goto(page);
    }
  };

  last = (): void => {
    if (this.pager.page != this.pager.pages - 1) {
      this.goto(this.pager.pages - 1);
    }
  };

  next = (): void => {
    const capacity = (this.pager.page + 1) * this.pager.limit;
    if (capacity < this.pager.total) {
      this.goto(this.pager.page + 1);
    }
  };

  goto = (page: number = 0): void => {
    this.pager.range(page);
    this.load.emit({ page: page, limit: this.pager.limit });
  };

  itemsPerPage = (): any => {
    this.pager.reset(this.pager.total, this.pager.limit);
  }

  ngOnDestroy = () => this.subscription?.unsubscribe();
}
