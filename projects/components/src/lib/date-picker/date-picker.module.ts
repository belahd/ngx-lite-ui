import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DatePicker } from './date-picker';
import { PopperModule } from '../popper/popper.module';

const EXPORTED_DECLARATIONS = [DatePicker];

@NgModule({
  imports: [CommonModule, PopperModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class DatePickerModule { }
