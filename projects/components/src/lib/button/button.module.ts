import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Button } from './button';

const EXPORTED_DECLARATIONS = [Button];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class ButtonModule {}
