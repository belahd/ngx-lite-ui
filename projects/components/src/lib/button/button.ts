import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-button',
  templateUrl: 'button.html',
  styleUrls: ['button.scss'],
})
export class Button {
  @Input() type: string = 'primary';
  @Input() width: string = '';
  @Input() disabled: boolean = false;
  @Input() rounded: boolean = true;
}
