import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Avatar } from './avatar';

const EXPORTED_DECLARATIONS = [Avatar];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class AvatarModule { }
