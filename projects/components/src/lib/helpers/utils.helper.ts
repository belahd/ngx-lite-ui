import { debounceTime, distinctUntilChanged, Subject } from 'rxjs';

export default class UtilsHelper {
  static debounce<T>(
    subject: Subject<T>,
    //request: (data: T) => void,
    response: (data: T) => void,
    delay: number = 100
  ) {
    return subject
      .pipe(
        debounceTime(delay),
        distinctUntilChanged()
        //switchMap((data: T) => request(data))
      )
      .subscribe((data: T) => response(data));
  }
}
