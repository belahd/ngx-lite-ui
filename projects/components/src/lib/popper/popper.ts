import { DOCUMENT } from '@angular/common';
import {
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { fromEvent, map, Subscription, takeWhile } from 'rxjs';
import { PopperService } from './popper.service';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'ngx-popper',
  templateUrl: 'popper.html',
  styleUrls: ['popper.scss'],
})
export class Popper implements OnDestroy {
  closeSubscription: Subscription = {} as Subscription;

  @ViewChild('popperRef') popperRef: ElementRef<HTMLDivElement> =
    {} as ElementRef<HTMLDivElement>;

  private readonly _ID: string = uuidv4();
  bottom: number | null = null;

  @Input() isOpen: boolean = false;
  @Input() isLoading: boolean = false;
  @Input() height: number = 250;
  @Input() width: string = 'auto';

  @Output() open = new EventEmitter();
  @Output() close = new EventEmitter();
  @Output() isOpenChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private elementRef: ElementRef<HTMLElement>,
    public popperService: PopperService
  ) {}

  private handleClose(event: Event) {
    this.popperService.id = '';
    this.bottom = null;

    this.isOpen = false;
    this.close.emit(event);
    this.isOpenChange.emit(this.isOpen);
  }

  private handleOpen(event: Event) {
    this.popperService.id = this._ID;

    const rect =
      this.elementRef.nativeElement?.firstElementChild?.getBoundingClientRect();
    if (rect) {
      this.bottom =
        window.innerHeight - rect.bottom < this.height ? rect.height : null;
    }

    this.isOpen = true;
    this.isOpenChange.emit(this.isOpen);
    this.open.emit(event);
  }

  toggle(event: MouseEvent) {
    if (this.isLoading) {
      return;
    }

    if (this.popperService.id && this.popperService.id !== this._ID) {
      this.popperService.id = this._ID;
      return;
    }

    event.stopPropagation();

    if (this.isOpen) {
      this.handleClose(event);
      return;
    }

    this.closeSubscription = fromEvent(this.document, 'click')
      .pipe(
        map((event: any) => ({
          close:
            event.target &&
            !this.popperRef?.nativeElement.contains(event.target),
        })),
        takeWhile(({ close }) => {
          if (close) {
            this.handleClose(event);
          }

          return !close;
        })
      )
      .subscribe();

    this.handleOpen(event);
  }

  ngOnDestroy() {
    this.closeSubscription?.unsubscribe?.();
  }
}
