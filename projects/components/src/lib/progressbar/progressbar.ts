import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-progressbar',
  templateUrl: 'progressbar.html',
  styleUrls: ['progressbar.scss'],
})
export class Progressbar {
  @Input() color: string = '_default';
  @Input() width: string = '100%';
  @Input() value: number = 0;

  percent = () => -Math.max(100 - this.value, 0) + '%';
}
