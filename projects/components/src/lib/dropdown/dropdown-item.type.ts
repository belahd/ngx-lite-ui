export interface DropdownItem {
  id?: number | string;
  text: string;
  icon?: string;
  click?: (item: DropdownItem) => void;
  selected?: boolean;
}
