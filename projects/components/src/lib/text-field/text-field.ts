import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ngx-text-field',
  templateUrl: 'text-field.html',
  styleUrls: ['text-field.scss'],
})
export class TextField {
  isFocused: boolean = false;

  @Input() label: string = '';
  @Input() required: boolean = false;
  @Input() type: string = 'text';
  @Input() value: string = '';
  @Input() placeholder: string = '';
  @Input() maxLength: number = 1000;
  @Input() disabled: boolean = false;
  @Input() visible: boolean = true;
  @Input() error: string = '';

  @Output() valueChange = new EventEmitter<string>();
  @Output() errorChange = new EventEmitter<string>();

  onKeypress = () => (this.error = '');

  onFocus = () => (this.isFocused = true);

  onBlur = () => (this.isFocused = false);

  onChange = (event: any) => {
    this.value = event.target.value;
    this.valueChange.emit(this.value ?? '');

    this.error = '';
    this.errorChange.emit('');
  };
}
