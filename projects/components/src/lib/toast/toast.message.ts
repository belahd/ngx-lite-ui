import { v4 as uuidv4 } from 'uuid';

export class ToastMessage {
  private _id: string;

  constructor(public message: any) {
    this._id = uuidv4();
  }

  public get id() {
    return this._id;
  }

  private set id(v: string) {
    this._id = v;
  }
}
