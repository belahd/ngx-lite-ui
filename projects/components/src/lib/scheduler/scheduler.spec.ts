import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Scheduler } from './scheduler';

describe('Scheduler', () => {
  let component: Scheduler;
  let fixture: ComponentFixture<Scheduler>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Scheduler],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Scheduler);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
