import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DropdownMenu } from './dropdown-menu';
import { PopperModule } from '../popper/popper.module';
import { SharedModule } from './shared.module';

const EXPORTED_DECLARATIONS = [DropdownMenu];

@NgModule({
  imports: [CommonModule, SharedModule, PopperModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class DropdownMenuModule {}
