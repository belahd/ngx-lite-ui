import { Component, ContentChildren, OnInit, QueryList } from '@angular/core';
import { from, Observable } from 'rxjs';
import { Tab } from './tab';

@Component({
  selector: 'ngx-tabs',
  templateUrl: 'tabs.html',
  styleUrls: ['tab.scss'],
})
export class Tabs implements OnInit {
  @ContentChildren(Tab) tabs!: QueryList<Tab>;
  items!: Observable<Tab[]>;
  selectedTab!: Tab;

  select = (tab: Tab) => {
    if (this.selectedTab === tab) {
      return;
    }

    this.selectedTab.isSelected = false;

    tab.isSelected = true;
    this.selectedTab = tab;
  }

  ngOnInit(): void {
    this.items = from(Promise.resolve().then(() => this.tabs.toArray()));
    Promise.resolve().then(() => this.selectedTab = this.tabs.first);
  }
}
