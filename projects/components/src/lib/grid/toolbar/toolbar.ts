import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { DropdownItem } from '../../dropdown';

@Component({
  selector: 'ngx-grid-toolbar',
  templateUrl: 'toolbar.html',
  styleUrls: ['toolbar.scss'],
})
export class Toolbar implements OnInit {
  items: Array<DropdownItem> = [];

  @Input() columns: any[] = [];
  @Input() itemTemplate?: TemplateRef<Element>;

  @Output() reset: EventEmitter<any> = new EventEmitter();
  @Output() search: EventEmitter<any> = new EventEmitter();
  @Output() export: EventEmitter<any> = new EventEmitter();
  @Output() columnVisibilityChange: EventEmitter<any> = new EventEmitter();

  onColumnChanged = (item: any): void => {
    const column = this.columns.find((x) => x.name === item.id);
    column.visible = item.selected;
    this.columnVisibilityChange.emit(column);
  };

  onSearch = (event: any): void => this.search.emit(event.target.value);

  onReset = (): void => this.reset.emit();

  onExport = (): void => this.export.emit();

  ngOnInit(): void {
    this.items = this.columns?.map((x) => ({
      id: x.name,
      text: x.caption,
      selected: x.visible,
    }));
  }
}
