import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[draggingHandler]',
})
export class DraggableHandlerDirective {
  constructor(public elementRef: ElementRef<HTMLElement>) {}
}
