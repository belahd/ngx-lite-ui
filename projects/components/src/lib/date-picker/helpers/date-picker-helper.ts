export default class DateTimeHelper {
    static daysInMonth = (year: number, month: number) => new Date(year, month, 0).getDate();

    static monthFirstDay = (year: number, month: number) => new Date(year, month, 1).getDay();

    static getMonthDays = (year: number, month: number) => {
        const days = this.daysInMonth(year, month + 1);
        const firstDay = this.monthFirstDay(year, month);

        const n = (firstDay + days) / 7;
        const countWeeks = Math.floor(n) + (n - Math.floor(n) > 0 ? 1 : 0);
        const calendar = Array(countWeeks * 7).fill(null);
        for (let i = firstDay; i < days + firstDay; ++i) {
            calendar[i] = i - firstDay + 1;
        }

        return calendar.reduce((rows, day, index) =>
            (index % 7 === 0 ?
                rows.push([day ? new Date(year, month, day) : null]) :
                rows[rows.length - 1].push(day ? new Date(year, month, day) : null)) && rows, []);
    }

    static yearRange = (year: number): number[][] => {
        const n = Math.floor(year / 10) * 10 - 1;
        const years: any[] = [0, ...Array.from({ length: 10 }, (_, i) => n + i + 1), 0];
        return this.reshape(years, 4);
    }

    static reshape = (data: any[], columns: number): any[][] =>
        data.reduce((rows, item, index) => (index % columns === 0 ? rows.push([item]) : rows[rows.length - 1].push(item)) && rows, []);
}