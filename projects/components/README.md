# Angular Lightweight UI Components Library

[![NPM version][npm-image]][npm-url]
[![Downloads][downloads-image]][downloads-url]

A free lightweight UI library for Angular enterprise applications. It contains a high-performance and responsive UI components.

A minimal demo page can be found <a href="https://belahd.github.io/" target="_blank">here</a>.

### Installation

```
// The package can be installed via NPM
$ npm i ngx-lite-ui --save

// Or by using Yarn
$ yarn add ngx-lite-ui
```

### Features

- [x] Label
- [x] TextField
- [x] Dropdown
- [x] Button
- [x] Checkbox
- [x] Radio button
- [x] Switch
- [x] Progressbar
- [x] Icon
- [x] Cards
- [x] Card
- [x] Bubble
- [x] Avatar
- [x] Tab
- [x] Dropdown menu
- [x] Toast
- [x] Grid
- [x] Date picker
- [x] popup
- [x] loading state
- [ ] Spreadsheet
- [ ] Scheduler
- [ ] etc

### Requirements

Add the following links inside the head element of your html page.

```html
<head>
  ...
  <link
    rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
  />
  <link
    href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined"
    rel="stylesheet"
  />
  ...
</head>
```

### Getting started

This is a basic usage of ngx-label. To import the existing LabelModule into your project (ex: app.module), proceed as follows:

```Javascript
import { LabelModule } from 'ngx-lite-ui';

imports: [
    ...
    LabelModule,
    ...
  ],
```

Add the following line to your component:

```html
<ngx-label [required]="true">This is a required label</ngx-label>
```

Follow the same process for the other components.

### Contribute

We are always open to your suggestions for further improvement.

### Change log

### License

[MIT](LICENSE)

[npm-image]: https://img.shields.io/npm/v/ngx-lite-ui.svg?style=flat-square
[npm-url]: https://www.npmjs.com/package/ngx-lite-ui
[downloads-image]: https://img.shields.io/npm/dm/ngx-lite-ui.svg?style=flat-square
[downloads-url]: https://www.npmjs.com/package/ngx-lite-ui
