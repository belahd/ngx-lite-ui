import { AfterViewInit, Directive, ElementRef, Inject, Output, } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { distinctUntilChanged, map, switchMap, takeUntil, tap, } from 'rxjs/operators';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[resizer]',
})
export class ResizerDirective implements AfterViewInit {
  @Output() readonly resizer = fromEvent<MouseEvent>(
    this.resizerRef.nativeElement,
    'mousedown'
  ).pipe(
    tap((event) => {
      event.preventDefault();
      event.stopPropagation();
    }),
    switchMap(() => {
      const rect =
        this.resizerRef.nativeElement?.parentElement?.parentElement?.getBoundingClientRect() ?? this.resizerRef.nativeElement.getBoundingClientRect();
      return fromEvent<MouseEvent>(this.document, 'mousemove').pipe(
        map((event) => rect.width + event.clientX - rect.right),
        distinctUntilChanged(),
        takeUntil(fromEvent(this.document, 'mouseup'))
      );
    })
  );

  constructor(@Inject(DOCUMENT) private document: Document, private resizerRef: ElementRef<HTMLElement>) { }

  ngAfterViewInit(): void {
    const rect = this.resizerRef.nativeElement?.parentElement?.parentElement?.getBoundingClientRect();
    if (rect) {
      this.resizerRef.nativeElement?.parentElement?.parentElement?.setAttribute('style', `width: ${rect.width}`);
    }
  }
}
