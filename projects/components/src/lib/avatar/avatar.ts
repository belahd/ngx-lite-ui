import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-avatar',
  templateUrl: 'avatar.html',
  styleUrls: ['avatar.scss'],
})
export class Avatar {
  @Input() src: string = '';
  @Input() color: string = "#FFFFFF";
  @Input() bgColor: string = "#22d0b8";
  @Input() name: string = "";
  @Input() size: number = 24;
}
