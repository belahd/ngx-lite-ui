import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Cards } from './cards';
import { Card } from './card';
import { CardHeader } from './card-header';
import { CardBody } from './card-body';
import { CardFooter } from './card-footer';

const EXPORTED_DECLARATIONS = [Cards, Card, CardHeader, CardBody, CardFooter];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class CardModule {}
