import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ngx-checkbox',
  templateUrl: 'checkbox.html',
  styleUrls: ['checkbox.scss'],
})
export class Checkbox {
  @Input() name: string = 'checkbox';
  @Input() checked: boolean = false;
  @Input() disabled: boolean = false;
  @Input() color: string = '_default';
  @Input() toggleable: boolean = true;

  @Output() checkedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  toggle = (event: any): void => {
    if (this.toggleable && !this.disabled) {
      //event.stopPropagation();
      this.checked = !this.checked;
      this.checkedChange.emit(this.checked);
    }
  };
}
