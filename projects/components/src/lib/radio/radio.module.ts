import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Radio } from './radio';

const EXPORTED_DECLARATIONS = [Radio];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class RadioModule { }
