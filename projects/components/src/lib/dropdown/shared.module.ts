import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DropdownItems } from './dropdown-items';

@NgModule({
  imports: [CommonModule],
  declarations: [DropdownItems],
  exports: [DropdownItems],
})
export class SharedModule {}
