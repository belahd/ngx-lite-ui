import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { DropdownItem } from '../dropdown';

type PagerType = {
  page: number;
  limit: number;
  total: number;
  from: number;
  to: number;
  pages: number;
  limits: Array<DropdownItem>;
};

@Injectable({ providedIn: 'root' })
export class PagerService {
  private data: PagerType = {
    page: 0,
    limit: 10,
    total: 0,
    from: 0,
    to: 0,
    pages: 0,
    limits: [
      { id: 10, text: '10' },
      { id: 15, text: '15' },
      { id: 25, text: '25' },
      { id: 50, text: '50' },
    ],
  };

  subject: Subject<number> = new Subject<number>();

  get page() {
    return this.data.page;
  }

  get pages() {
    return this.data.pages;
  }

  get total() {
    return this.data.total;
  }

  set limit(n: any) {
    this.data.limit = n;
    this.reset(this.total, n);
    this.load(this.data.page);
  }
  get limit() {
    return this.data.limit;
  }
  get defaultLimit() {
    return parseInt(this.data.limits?.[0]?.id?.toString() ?? '10');
  }

  get from() {
    return this.data.from;
  }

  get to() {
    return this.data.to;
  }

  get limits() {
    return this.data.limits;
  }

  reset = (total: number, limit: number = this.defaultLimit): void => {
    this.data.page = 0;
    this.data.total = total < 0 ? 0 : total;
    this.data.limit = limit;
    const pages = this.total / this.limit;
    this.data.pages =
      Math.floor(pages) + (pages - Math.floor(pages) > 0 ? 1 : 0);

    this.range(this.data.page);
  };

  range = (page: number) => {
    this.data.page = page < 0 ? 0 : page;
    this.data.from =
      this.data.page * this.data.limit + (this.data.total > 0 ? 1 : 0);
    this.data.to =
      (this.data.page + 1) * this.data.limit < this.data.total
        ? (this.data.page + 1) * this.data.limit
        : this.data.total;
  };

  load = (page: number) => {
    this.subject.next(page);
  };

  get message(): Observable<number> {
    return this.subject.asObservable();
  }
}
