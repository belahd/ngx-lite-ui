import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DraggableDirective } from './draggable.directive';
import { DraggableHandlerDirective } from './draggable-handler.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [
    DraggableDirective,
    DraggableHandlerDirective,
  ],
  exports: [DraggableDirective, DraggableHandlerDirective],
})
export class SharedModule {}
