import { TemplateRef } from '@angular/core';

export type ColumnType = {
  name: string;
  caption: string;
  type: string;
  format: string;
  readable: boolean;
  focus: boolean;
};
