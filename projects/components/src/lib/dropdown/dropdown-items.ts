import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  TemplateRef,
} from '@angular/core';
import { DropdownItem } from './dropdown-item.type';

@Component({
  selector: 'ngx-dropdown-items',
  templateUrl: 'dropdown-items.html',
  styleUrls: ['dropdown.scss'],
})
export class DropdownItems implements OnInit {
  showIconColumn: boolean = false;

  @Input() items: Array<DropdownItem> = [];
  @Input() selected: number | string | undefined | null;
  @Input() itemTemplate?: TemplateRef<Element>;

  @Output() select: EventEmitter<DropdownItem> =
    new EventEmitter<DropdownItem>();

  click(item: DropdownItem) {
    this.selected = item.id;
    item.click?.(item);
    this.select.emit(item);
  }

  ngOnInit(): void {
    this.showIconColumn = this.items?.some((x) => x.icon);
  }
}
