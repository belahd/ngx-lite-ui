import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-card',
  template: `<div class="card" [style.width]="width" [style.height]="height">
    <div [ngClass]="css">
      <ng-content select="ngx-card-header"></ng-content>
      <ng-content select="ngx-card-body"></ng-content>
      <ng-content select="ngx-card-footer"></ng-content>
    </div>
  </div>`,
  styleUrls: ['card.scss'],
})
export class Card {
  @Input() width: string = '100%';
  @Input() height: string = '100%';
  @Input() css: string = '';
}
