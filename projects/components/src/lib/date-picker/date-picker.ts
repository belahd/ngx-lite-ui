import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { CalendarEnum } from './helpers/date-picker-enum';
import DateTimeHelper from './helpers/date-picker-helper';

@Component({
  selector: 'ngx-date-picker',
  templateUrl: 'date-picker.html',
  styleUrls: ['date-picker.scss'],
})
export class DatePicker {
  config = {
    year: {
      min: 1910,
      max: 2090,
    },
    days: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    months: [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ],
    monthsShort: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ],
  };

  type: CalendarEnum = CalendarEnum.date;
  header: string = '';
  data: any;
  year: number = new Date().getFullYear();
  month: number = new Date().getMonth();
  today: Date = new Date(this.year, this.month, new Date().getDate());

  isOpen: boolean = false;

  @Input() value!: Date | null;
  @Input() format: string = '';
  @Input() className: string = '';
  @Output() valueChange: EventEmitter<Date | null> =
    new EventEmitter<Date | null>();

  refresh = (type: CalendarEnum): void => {
    this.type = type;
    switch (type) {
      case CalendarEnum.date:
        this.header = `${this.config.months[this.month]} ${this.year}`;
        this.data = DateTimeHelper.getMonthDays(this.year, this.month);
        break;

      case CalendarEnum.month:
        this.header = `${this.year}`;
        this.data = DateTimeHelper.reshape(this.config.monthsShort, 4);
        break;

      case CalendarEnum.year:
        const year = Math.floor(this.year / 10) * 10;
        this.header = `${year} - ${year + 9}`;
        this.data = DateTimeHelper.yearRange(this.year);
        break;
    }
  };

  reset = (): void => {
    this.year = (this.value ?? this.today).getFullYear();
    this.month = (this.value ?? this.today).getMonth();

    this.refresh(CalendarEnum.date);
  };

  onSelectDate = (date: any): void => {
    this.isOpen = false;
    this.year = date.getFullYear();
    this.month = date.getMonth();
    this.value = new Date(this.year, this.month, date.getDate());
    this.valueChange.emit(this.value);
  };

  onSelectMonth = (event: MouseEvent, month: number): void => {
    event.stopPropagation();

    this.month = month;
    this.refresh(CalendarEnum.date);
  };

  onSelectYear = (event: MouseEvent, year: number): void => {
    event.stopPropagation();

    this.year = year;
    this.refresh(CalendarEnum.month);
  };

  onToday = (): void => {
    this.onSelectDate(this.today);
    this.refresh(CalendarEnum.date);
  };

  onClear = (): void => {
    this.value = null;
    this.valueChange.emit(this.value);
    this.reset();
  };

  onPrevious = (): void => {
    if (this.year <= this.config.year.min) {
      return;
    }

    switch (this.type) {
      case CalendarEnum.date:
        this.year = this.month > 0 ? this.year : this.year - 1;
        this.month = this.month > 0 ? this.month - 1 : 11;
        this.refresh(this.type);
        break;

      case CalendarEnum.month:
        this.year = this.year + 1;
        break;

      case CalendarEnum.year:
        this.year = this.year - 10;
        this.refresh(this.type);
        break;
    }
  };

  onNext = (): void => {
    if (this.year >= this.config.year.max) {
      return;
    }

    switch (this.type) {
      case CalendarEnum.date:
        this.year = this.month < 11 ? this.year : this.year + 1;
        this.month = this.month < 11 ? this.month + 1 : 0;
        this.refresh(this.type);
        break;

      case CalendarEnum.month:
        this.year = this.year - 1;
        break;

      case CalendarEnum.year:
        this.year = this.year + 10;
        this.refresh(this.type);
        break;
    }
  };

  onChange = (): void => {
    if (this.type === CalendarEnum.year) {
      return;
    }

    this.refresh(
      this.type === CalendarEnum.date
        ? CalendarEnum.month
        : this.type === CalendarEnum.month
        ? CalendarEnum.year
        : CalendarEnum.date
    );
  };
}
