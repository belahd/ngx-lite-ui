import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Icon } from './icon';

const EXPORTED_DECLARATIONS = [Icon];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class IconModule {}
