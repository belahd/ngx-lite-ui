export default class Helper {
    static convertToCSV = (data: any): string => {
        var array = data;
        var str = '';

        for (var i = 0; i < array?.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        return str;
    }
}