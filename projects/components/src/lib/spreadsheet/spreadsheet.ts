import { Component, Input, QueryList, ViewChildren } from '@angular/core';
import { ColumnType } from './helpers/spreadsheet-types';
import { Cell } from './cell'

@Component({
  selector: 'ngx-spreadsheet',
  templateUrl: 'spreadsheet.html',
  styleUrls: ['spreadsheet.scss'],
  host: { '(document:keydown)': 'navigate($event)' },
})
export class Spreadsheet {
  @ViewChildren('cell') cells!: QueryList<Cell>;
  cell!: any;
  focus: boolean = false;
  selected!: { row: number, col: number };

  @Input() columns: ColumnType[] = [];
  @Input() rows: any = [];

  onFocus = (row: number, col: number): void => {
    console.log(row, col);
    if (this.cell) {
      this.cell.focus = false;
    }
    this.cell = this.cells.find((_, i) => i === row * this.columns.length + col);
    this.cell.focus = !this.cell.focus;
    this.selected = { row, col }
  }

  navigate = (event: any) => {
    event.preventDefault();

    if (!this.selected) {
      this.selected = { row: 0, col: 0 };
      return;
    }

    switch (event.keyCode) {
      case 37: //left
        const n = this.columns.slice(0, this.selected.col).reverse().findIndex(c => c.readable);
        this.selected.col = Math.max(this.selected.col - 1 - (n > 0 ? n : 0), 0);
        break;

      case 38: // up
        this.selected.row = Math.max(0, this.selected.row - 1);
        break;

      case 39: // right
        const m = this.columns.slice(this.selected.col + 1).findIndex(c => c.readable);
        this.selected.col = Math.min(this.selected.col + 1 + (m > 0 ? m : 0), this.columns.length - 1);
        break;

      case 40: // down
        this.selected.row = Math.min(this.selected.row + 1, this.rows.length - 1);
        break;
    }

    this.onFocus(this.selected.row, this.selected.col);
  }
}
