import { Component, TemplateRef, ViewChild } from "@angular/core";

@Component({
  selector: "ngx-tab-label",
  template: "<ng-template><ng-content></ng-content></ng-template>",
})
export class TabLabel {
  @ViewChild(TemplateRef) template!: TemplateRef<any>;
}