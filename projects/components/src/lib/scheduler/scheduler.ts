import { Component } from '@angular/core';

@Component({
  selector: 'ngx-scheduler',
  templateUrl: 'scheduler.html',
  styleUrls: ['scheduler.scss'],
})
export class Scheduler {}
