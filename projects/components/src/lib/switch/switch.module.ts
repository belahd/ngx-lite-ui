import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Switch } from './switch';

const EXPORTED_DECLARATIONS = [Switch];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class SwitchModule {}
