import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Dropdown } from './dropdown';
import { PopperModule } from '../popper/popper.module';
import { SharedModule } from './shared.module';
import { LabelModule } from '../label/label.module';

const EXPORTED_DECLARATIONS = [Dropdown];

@NgModule({
  imports: [CommonModule, SharedModule, PopperModule, LabelModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class DropdownModule {}
