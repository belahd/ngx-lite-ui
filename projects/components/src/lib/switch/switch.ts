import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ngx-switch',
  templateUrl: 'switch.html',
  styleUrls: ['switch.scss'],
})
export class Switch {
  @Input() color: string = '_default';
  @Input() selected: boolean = false;
  @Input() disabled: boolean = false;

  @Output() selectedChange = new EventEmitter();

  onClick = () => {
    if (!this.disabled) {
      this.selected = !this.selected;
      this.selectedChange.emit(this.selected);
    }
  };
}
