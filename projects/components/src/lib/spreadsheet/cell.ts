import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'ngx-cell',
  templateUrl: 'cell.html',
  styleUrls: ['cell.scss'],
})
export class Cell {

  @Input() value!: string | number | Date | null;
  @Input() focus: boolean = false;

  @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() down: EventEmitter<any> = new EventEmitter<any>();

  shiftFocusDown = () => {
    // console.log('down');
    // this.down.emit();
  }

  onClick = (event: any) => event.stopPropagation();

  onBlur = (event: any) => {
    this.value = event.target.value;
    this.valueChange.emit(this.value);
  }
}
