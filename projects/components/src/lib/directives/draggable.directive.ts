import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  Directive,
  ElementRef,
  ContentChild,
  Inject,
  Input,
} from '@angular/core';
import { fromEvent, map, switchMap, takeUntil } from 'rxjs';
import { DraggableHandlerDirective } from './draggable-handler.directive';

@Directive({
  selector: '[draggable]',
})
export class DraggableDirective implements AfterViewInit {
  @ContentChild(DraggableHandlerDirective) handler =
    {} as DraggableHandlerDirective;

  boundingElement: HTMLElement | HTMLBodyElement = {} as
    | HTMLElement
    | HTMLBodyElement;
  element: HTMLElement = {} as HTMLElement;
  handlerElement: HTMLElement = {} as HTMLElement;

  @Input() boundaryQuery: string = 'body';

  position = {
    x: 0,
    y: 0,
    clientX: 0,
    clientY: 0,
    deltaX: 0,
    deltaY: 0,
  };

  constructor(
    private elementRef: ElementRef,
    @Inject(DOCUMENT) private document: any
  ) {}

  isReady() {
    this.element = this.elementRef.nativeElement as HTMLElement;
    if (!this.element) {
      return false;
    }

    this.boundingElement = this.document.querySelector(this.boundaryQuery);
    if (!this.boundingElement) {
      return false;
    }

    this.handlerElement = this.handler?.elementRef?.nativeElement;
    if (!this.handlerElement) {
      return;
    }

    return true;
  }

  getBoundingSize(): {
    w: number;
    h: number;
  } {
    return {
      w: this.boundingElement.offsetWidth,
      h: this.boundingElement.offsetHeight,
    };
  }

  getElementRect(): {
    x: number;
    y: number;
    w: number;
    h: number;
  } {
    return {
      x: this.element.offsetLeft,
      y: this.element.offsetTop,
      w: this.element.offsetWidth,
      h: this.element.offsetHeight,
    };
  }

  ngAfterViewInit() {
    if (!this.isReady()) {
      return;
    }

    fromEvent(this.handlerElement, 'mousedown')
      .pipe(
        switchMap((start: any) => {
          start.preventDefault();

          const elementRect = this.getElementRect();
          const boundingSize = this.getBoundingSize();

          this.position.clientX = start.clientX;
          this.position.clientY = start.clientY;

          return fromEvent(this.document, 'mousemove').pipe(
            map((move: any) => {
              move.preventDefault();

              this.position.deltaX = this.position.clientX - move.clientX;
              this.position.deltaY = this.position.clientY - move.clientY;
              this.position.clientX = move.clientX;
              this.position.clientY = move.clientY;

              const x = this.position.x - this.position.deltaX;
              const y = this.position.y - this.position.deltaY;

              return {
                x:
                  x < -elementRect.x
                    ? -elementRect.x
                    : x + elementRect.x + elementRect.w > boundingSize.w
                    ? boundingSize.w - elementRect.x - elementRect.w
                    : x,

                y:
                  y < -elementRect.y
                    ? -elementRect.y
                    : y + elementRect.y + elementRect.h > boundingSize.h
                    ? boundingSize.h - elementRect.y - elementRect.h
                    : y,
              };
            }),
            takeUntil(fromEvent(this.document, 'mouseup'))
          );
        })
      )
      .subscribe((move) => {
        this.position.x = move.x;
        this.position.y = move.y;
        this.element.style.transform = `translate3d(${move.x}px, ${move.y}px, 0)`;
      });
  }
}
