import { Component, TemplateRef, ViewChild } from "@angular/core";

@Component({
  selector: "ngx-tab-content",
  template: "<ng-template><ng-content></ng-content></ng-template>",
})
export class TabContent {
  @ViewChild(TemplateRef) template!: TemplateRef<any>;
}