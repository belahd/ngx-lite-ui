import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Bubble } from './bubble';

const EXPORTED_DECLARATIONS = [Bubble];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class BubbleModule { }
