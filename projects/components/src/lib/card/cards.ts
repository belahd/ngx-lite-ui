import { Component } from '@angular/core';

@Component({
  selector: 'ngx-cards',
  template: '<div class="cards"><ng-content></ng-content></div>',
  styles: [
    `
      .cards {
        display: flex;
        flex-wrap: wrap;
        row-gap: 20px;
        column-gap: 20px;
      }
    `,
  ],
})
export class Cards {}
