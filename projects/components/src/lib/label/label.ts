import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-label',
  templateUrl: './label.html',
  styleUrls: ['./label.scss'],
})
export class Label {
  @Input() required: boolean = false;
  @Input() disabled: boolean = false;
  @Input() visible: boolean = true;
}
