import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from '../../icon/icon.module';
import { Toolbar } from './toolbar';
import { DropdownMenuModule } from '../../dropdown/dropdown-menu.module';
import { CheckboxModule } from '../../checkbox/checkbox.module';

const EXPORTED_DECLARATIONS = [Toolbar];

@NgModule({
  imports: [CommonModule, IconModule, DropdownMenuModule, CheckboxModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class ToolbarModule {}
