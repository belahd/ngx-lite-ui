import {
  Directive,
  Input,
  SimpleChanges,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { Skeleton } from './skeleton';

@Directive({
  selector: '[skeleton]',
})
export class SkeletonDirective {
  @Input('skeleton') isLoading: boolean = true;
  @Input('skeletonRepeat') size: number = 1;
  @Input('skeletonDynamic') dynamic: boolean = false;
  @Input('skeletonWidth') width: string = '';
  @Input('skeletonHeight') height: string = '';
  @Input('skeletonClassName') className: string = '';
  @Input('skeletonType') type: string = 'rect';

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (!changes['isLoading']) {
      return;
    }

    this.viewContainer.clear();

    if (!changes['isLoading'].currentValue) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      return;
    }

    Array.from({ length: this.size }).forEach(() => {
      const ref = this.viewContainer.createComponent(Skeleton);

      Object.assign(ref.instance, {
        width: this.dynamic ? `${this.random(20, 95)}%` : this.width,
        height: this.height,
        className: this.className,
        type: this.type,
      });
    });
  }

  random = (min: number, max: number) =>
    Math.floor(Math.random() * (max - min + 1) + min);
}
