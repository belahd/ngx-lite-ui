import { Component, ElementRef } from '@angular/core';

@Component({
  selector: 'loading-state',
  styleUrls: ['./skeleton.scss'],
  template: `<div
    class="pulse {{ className }}"
    [style.width]="width"
    [style.height]="height"
    [ngClass]="type"
  ></div>`,
})
export class Skeleton {
  width: string = '100px';
  height: string = '15px';
  className: string = '';
  type: string = 'rect';

  constructor(private host: ElementRef<HTMLElement>) {}
}
