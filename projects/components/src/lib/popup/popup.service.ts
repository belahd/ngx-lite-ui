import { ApplicationRef, ComponentRef, Injectable, Type } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PopupService {
  popupRef: ComponentRef<any> | null = null;

  constructor(private appRef: ApplicationRef) {}

  public open(component: Type<any>, config: any) {
    if (this.popupRef === null) {
      this.popupRef =
        this.appRef.components?.[0]?.instance?.viewContainerRef?.createComponent(
          component
        );
    }

    if (this.popupRef) {
      //this.popupRef.instance.params = 'hhhh';
    }
  }

  public close() {
    this.popupRef?.destroy();
    this.popupRef = null;
  }
}
