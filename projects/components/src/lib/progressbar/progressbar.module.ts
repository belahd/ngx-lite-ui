import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Progressbar } from './progressbar';

const EXPORTED_DECLARATIONS = [Progressbar];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class ProgressbarModule {}
