import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from '../button/button.module';
import { Popup } from './popup';
import { SharedModule } from '../directives/shared.module';

const EXPORTED_DECLARATIONS = [Popup];

@NgModule({
  imports: [CommonModule, ButtonModule, SharedModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class PopupModule {}
