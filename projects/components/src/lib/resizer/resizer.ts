import { Component, ElementRef, HostBinding } from "@angular/core";

@Component({
  selector: "[resizable]",
  template: `<div class="wrapper">
                <div class="content">
                  <ng-content></ng-content>
                </div>
                <div class="resizer" (resizer)="onResize($event)"></div>
              </div>`,
  styleUrls: ["./resizer.scss"],
})
export class Resizer {
  @HostBinding("style.width.px") width: number | null = null;

  onResize = (width: number): void => {
    this.width = width;
  }
}
