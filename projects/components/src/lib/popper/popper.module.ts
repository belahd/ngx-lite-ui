import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Popper } from './popper';
import { SharedModule } from '../directives/shared.module';
import { PopperService } from './popper.service';

const EXPORTED_DECLARATIONS = [Popper];

@NgModule({
  imports: [CommonModule, SharedModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
  providers: [PopperService],
})
export class PopperModule {}
