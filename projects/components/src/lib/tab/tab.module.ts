import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Tab } from './tab';
import { Tabs } from './tabs';
import { TabLabel } from './tab-label';
import { TabContent } from './tab-content';

const EXPORTED_DECLARATIONS = [Tabs, Tab, TabLabel, TabContent];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class TabModule { }
