import { Component } from '@angular/core';
import { ToastPosition } from './toast-position';
import { ToastType } from './toast-type';
import { TOGGLE } from './toast.animation';
import { ToastMessage } from './toast.message';
import { ToastService } from './toast.service';

@Component({
  selector: 'ngx-toast',
  templateUrl: 'toast.html',
  styleUrls: ['toast.scss'],
  animations: [TOGGLE],
})
export class Toast {
  messages: ToastMessage[] = [];

  _config: {
    duration: number;
    position: ToastPosition;
    type: ToastType;
  } = {
    duration: 3000,
    position: ToastPosition.BottomCenter,
    type: ToastType.Info,
  };

  public get config() {
    return this._config;
  }

  public set config(value: {
    duration: number;
    position: ToastPosition;
    type: ToastType;
  }) {
    this._config = { ...value };
  }

  public any() {
    return this.messages.length;
  }

  public add(message: ToastMessage) {
    this.messages.push(message);
  }

  public remove(id: string) {
    const index: number = this.messages.findIndex((e) => e.id === id) ?? -1;
    if (index > -1) {
      this.messages.splice(index, 1);
    }
  }
}
