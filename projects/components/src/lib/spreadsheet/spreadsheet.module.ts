import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Spreadsheet } from './spreadsheet';
import { Cell } from './cell';

const EXPORTED_DECLARATIONS = [Spreadsheet];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: [EXPORTED_DECLARATIONS, Cell],
})
export class SpreadsheetModule { }
