import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-card-footer',
  template: `<div [ngClass]="css">
    <div class="card-footer">
      <ng-content></ng-content>
    </div>
  </div>`,
  styleUrls: ['card.scss'],
})
export class CardFooter {
  @Input() css: string = '';
}
