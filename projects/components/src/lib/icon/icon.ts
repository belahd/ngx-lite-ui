import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ngx-icon',
  template: `<div
    class="material-icons-outlined icon"
    [class.disabled]="disabled"
    [class.color]="color"
    [class.hover]="isClickable && !disabled"
    [ngClass]="className"
  >
    {{ name }}
  </div>`,
  styleUrls: ['icon.scss'],
})
export class Icon implements OnInit {
  @Input() name: string = 'insert_emoticon';
  @Input() disabled: boolean = false;
  @Input() color: string = '';
  @Input() className: string = '';

  isClickable: boolean = false;
  @Output() click: EventEmitter<any> = new EventEmitter<any>();

  onClick = (event: any): void => {
    event.stopPropagation();
    this.click.emit(event);
  };

  ngOnInit(): void {
    this.isClickable = this.click.observed;
  }
}
