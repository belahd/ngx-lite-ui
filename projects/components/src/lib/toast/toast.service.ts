import { Injectable, ComponentRef, ApplicationRef } from '@angular/core';
import { Toast } from './toast';
import { ToastPosition } from './toast-position';
import { ToastType } from './toast-type';
import { ToastMessage } from './toast.message';

@Injectable({ providedIn: 'root' })
export class ToastService {
  toastRef: ComponentRef<Toast> | null = null;

  constructor(private appRef: ApplicationRef) {}

  public show(
    message: string,
    config: {
      duration: number;
      position: ToastPosition;
      type: ToastType;
    } = {
      duration: 3000,
      position: ToastPosition.BottomCenter,
      type: ToastType.Default,
    }
  ): void {
    if (this.toastRef === null) {
      this.toastRef =
        this.appRef.components?.[0]?.instance?.viewContainerRef?.createComponent(
          Toast
        );
    }

    if (this.toastRef) {
      const msg = new ToastMessage(message);

      this.toastRef.instance.add(msg);
      this.toastRef.instance.config = config;

      setTimeout(() => {
        this.toastRef?.instance.remove(msg.id);

        if (!this.toastRef?.instance.any()) {
          this.toastRef?.destroy();
          this.toastRef = null;
        }
      }, config.duration);
    }
  }
}
