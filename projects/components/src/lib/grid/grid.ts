import {
  AfterContentInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  TemplateRef,
} from '@angular/core';
import { PagerService } from '../pager/pager.service';
import Helper from './helpers/grid-helper';
import { ColumnType } from './helpers/grid-types';

@Component({
  selector: 'ngx-grid',
  templateUrl: 'grid.html',
  styleUrls: ['grid.scss'],
})
export class Grid implements AfterContentInit, OnChanges {
  asc: string = 'ASCENDING';
  desc: string = 'DESCENDING';
  searchTerm: string = '';
  aggregate: any = null;
  visibleColumns: ColumnType[] = [];

  @Input() Columns: ColumnType[] = [];
  @Input() Items: any = [];
  @Input() total: number = 0;

  @Output() refresh: EventEmitter<any> = new EventEmitter();
  @Output() load: EventEmitter<{ page: number; limit: number; term: string }> =
    new EventEmitter();
  @Output() columnsVisibilityChanged: EventEmitter<any> = new EventEmitter();

  @Input() noDataTemplate?: TemplateRef<any>;

  constructor(public pager: PagerService) {}

  onLoad = (params: any): void =>
    this.load.emit({ ...params, term: this.searchTerm, order: this.aggregate });

  onReset = (): void => {
    this.pager.reset(this.total);
    this.pager.load(0);
  };

  onSearch = (term: string): void => {
    this.searchTerm = term;
    this.onLoad({ page: 0, limit: this.pager.limit });
  };

  sort = async (column: ColumnType) => {
    this.Columns.filter((x) => x.name !== column.name).forEach(
      (x: any) => (x.direction = '')
    );
    column.direction = !column.direction
      ? this.asc
      : column.direction === this.asc
      ? this.desc
      : '';

    this.aggregate = column.direction
      ? { name: column.name, direction: column.direction }
      : null;
    let params: any = {
      page: 0,
      limit: this.pager.limit,
      order: this.aggregate,
    };
    this.pager.reset(this.total, this.pager.limit);
    this.onLoad(params);
  };

  filter = async (event: any, column: ColumnType) => {
    event.stopPropagation();
  };

  onColumnVisibilityChange(): void {
    this.visibleColumns = this.Columns.filter((x: ColumnType) => x.visible);
  }

  ngAfterContentInit(): void {
    this.onColumnVisibilityChange();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['total']?.currentValue) {
      this.pager.reset(this.total, this.pager.limit);
    }
  }

  onExport = () => {
    var csv = Helper.convertToCSV(this.Items);

    var exportedFilenmae = 'data.csv';

    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });

    var link = document.createElement('a');
    if (link.download !== undefined) {
      var url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', exportedFilenmae);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  };
}
