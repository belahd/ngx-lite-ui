import {
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
} from '@angular/core';
import { DropdownItem } from './dropdown-item.type';

@Component({
  selector: 'ngx-dropdown-menu',
  templateUrl: 'dropdown-menu.html',
  styleUrls: ['dropdown.scss'],
})
export class DropdownMenu {
  isOpen: boolean = false;

  @Input() multiSelect: boolean = false;
  @Input() items: Array<DropdownItem> = [];
  @Input() itemTemplate?: TemplateRef<Element>;
  @Output() selectChanged: EventEmitter<any> = new EventEmitter();

  select(item: DropdownItem) {
    item.selected = !item.selected ?? true;
    this.selectChanged.emit(item);

    if (!this.multiSelect) {
      this.close();
    }
  }

  close() {
    this.isOpen = false;
  }
}
