import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PagerModule } from '../pager/pager.module';
import { Grid } from './grid';
import { ToolbarModule } from './toolbar/toolbar.module';
import { ResizerModule } from '../resizer/resizer.module';
const EXPORTED_DECLARATIONS = [Grid];

@NgModule({
  imports: [CommonModule, ToolbarModule, PagerModule, ResizerModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class GridModule { }
