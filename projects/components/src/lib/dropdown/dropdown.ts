import {
  Component,
  DoCheck,
  EventEmitter,
  Input,
  IterableDiffer,
  IterableDiffers,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import UtilsHelper from '../helpers/utils.helper';
import { DropdownItem } from './dropdown-item.type';

@Component({
  selector: 'ngx-dropdown',
  templateUrl: 'dropdown.html',
  styleUrls: ['dropdown.scss'],
})
export class Dropdown implements OnInit, DoCheck, OnDestroy {
  private differ!: IterableDiffer<any>;

  isOpen: boolean = false;
  searchValue: string = '';
  filteredItems: any = [];
  private readonly searchSubject = new Subject<string>();
  private searchSubscription?: Subscription;
  selectedItem: DropdownItem | undefined | null;

  @Input() label: string = '';
  @Input() required: boolean = false;
  @Input() isLoading: boolean = false;
  @Input() items: Array<DropdownItem> = [];
  @Input() readonly: boolean = false;
  @Input() disabled: boolean = false;
  @Input() isSearchable: boolean = false;
  @Input() searchPlaceholder: string = 'Search';
  @Input() isClearable: boolean = false;

  @Input() selected: number | string | undefined | null;
  @Output() selectedChange: EventEmitter<number | string | undefined | null> =
    new EventEmitter<number | string | undefined | null>();

  @Input() valueTemplate?: TemplateRef<Element>;
  @Input() itemTemplate?: TemplateRef<Element>;
  @Input() footerTemplate?: TemplateRef<Element>;
  @Input() noDataTemplate?: TemplateRef<Element>;

  constructor(private differs: IterableDiffers) {
    this.differ = this.differs.find(this.items).create(this.trackByFn);
  }

  select(item: DropdownItem) {
    this.searchValue = '';
    this.searchSubject.next(this.searchValue);
    this.isOpen = false;

    if (item.id === undefined || item.id !== this.selected) {
      this.selectedItem = item;
      this.selected = item.id;
      this.selectedChange.emit(this.selected);
    }
  }

  clear = (e?: Event): void => {
    e?.stopPropagation();

    this.selectedItem = null;
    this.selected = null;
  };

  search = (event: any) => {
    this.searchValue = (event.target as HTMLInputElement).value.trim();
    this.searchSubject.next(this.searchValue);
  };

  ngOnInit() {
    if (this.selected) {
      this.selectedItem = this.items.find((x) => x.id === this.selected);
    }

    this.searchSubscription = UtilsHelper.debounce<string>(
      this.searchSubject,
      (searchQuery) =>
        (this.filteredItems = searchQuery
          ? this.items.filter((item: any) =>
              item.text.toLowerCase().includes(searchQuery?.toLowerCase())
            )
          : [...this.items])
    );
  }

  ngDoCheck(): void {
    const changes = this.differ?.diff(this.items);
    if (changes) {
      this.searchValue = '';
      this.filteredItems = this.items ?? [];

      if (!this.filteredItems.length) {
        this.clear();
      } else if (
        !this.filteredItems.some((x: DropdownItem) => x.id === this.selected)
      ) {
        this.selected = this.filteredItems?.[0]?.id ?? null;
        this.selectedItem = this.filteredItems?.[0] ?? null;
      }
    }
  }

  trackByFn = (index: number, item: DropdownItem) => item.id;

  public ngOnDestroy(): void {
    this.searchSubscription?.unsubscribe();
  }
}
