import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Scheduler } from './scheduler';

const EXPORTED_DECLARATIONS = [Scheduler];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class SchedulerModule {}
