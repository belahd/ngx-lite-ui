import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Label } from './label';

const EXPORTED_DECLARATIONS = [Label];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class LabelModule {}
