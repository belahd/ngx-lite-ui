import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-card-body',
  template: `<div [ngClass]="css">
    <div class="card-body">
      <ng-content></ng-content>
    </div>
  </div>`,
  styleUrls: ['card.scss'],
})
export class CardBody {
  @Input() css: string = '';
}
