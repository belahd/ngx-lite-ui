import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ngx-radio',
  templateUrl: 'radio.html',
  styleUrls: ['radio.scss'],
})
export class Radio {
  @Input() value: string = 'radio';
  @Input() selected: string = '';
  @Input() disabled: boolean = false;
  @Input() color: string = '_default';

  @Output() selectedChange: EventEmitter<string> = new EventEmitter<string>();
  @Output() change: EventEmitter<string> = new EventEmitter<string>();

  onClick = (event: any): void => {
    if (!this.disabled) {
      event.stopPropagation();
      this.selected = this.value;
      this.selectedChange.emit(this.value);
      this.change.emit(this.value);
    }
  };
}
