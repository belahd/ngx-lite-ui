export enum ToastType {
  Default = '_default',
  Succuss = 'success',
  Info = 'info',
  Warning = 'warning',
  Danger = 'danger',
}
