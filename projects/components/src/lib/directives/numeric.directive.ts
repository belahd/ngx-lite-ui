import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[numeric]',
})
export class NumericDirective {
  @Input() decimals: number = 0;
  @Input() negative: number = 0;
  @Input() separator: string = '.';

  private checkAllowNegative(value: string) {
    if (this.decimals > 0) {
      var regExpString = `^-?\\s*((\\d+(\\${this.separator}\\d{0,${this.decimals}})?)|((\\d*(\\${this.separator}\\d{1,${this.decimals}}))))\\s*$`;
      return String(value).match(new RegExp(regExpString));
    } else {
      return String(value).match(new RegExp(/^-?\d+$/));
    }
  }

  private positive(value: string) {
    if (this.decimals > 0) {
      var regExpString = `^\\s*((\\d+(\\${this.separator}\\d{0,${this.decimals}})?)|((\\d*(\\${this.separator}\\d{1,${this.decimals}}))))\\s*$`;
      return String(value).match(new RegExp(regExpString));
    } else {
      return String(value).match(new RegExp(/^\d+$/));
    }
  }

  private process(value: string) {
    setTimeout(() => {
      let currentValue: string = this.element.nativeElement.value;
      let allowNegative = this.negative > 0 ? true : false;

      if (allowNegative) {
        if (
          !['', '-'].includes(currentValue) &&
          !this.checkAllowNegative(currentValue)
        ) {
          this.element.nativeElement.value = value;
        }
      } else {
        if (currentValue !== '' && !this.positive(currentValue)) {
          this.element.nativeElement.value = value;
        }
      }
    });
  }

  constructor(private element: ElementRef) {}

  @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
    this.process(this.element.nativeElement.value);
  }

  @HostListener('paste', ['$event'])
  onPaste(event: ClipboardEvent) {
    this.process(this.element.nativeElement.value);
  }
}
