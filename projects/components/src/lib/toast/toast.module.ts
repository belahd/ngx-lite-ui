import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Toast } from './toast';

const EXPORTED_DECLARATIONS = [Toast];

@NgModule({
  imports: [CommonModule],
  exports: EXPORTED_DECLARATIONS,
  declarations: EXPORTED_DECLARATIONS,
})
export class ToastModule {}
