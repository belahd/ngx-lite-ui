import { NgModule } from '@angular/core';
import { Resizer } from './resizer';
import { ResizerDirective } from './resizer.directive';

const EXPORTED_DECLARATIONS = [Resizer, ResizerDirective];

@NgModule({
    exports: [Resizer],
    declarations: EXPORTED_DECLARATIONS,
})
export class ResizerModule { }

